<?php
session_start();

//Exercice 2
echo '<h2>Exercice 2</h2>
    <p>Bonjour ' . $_SESSION['prenom'] . ' ' . $_SESSION['nom'] . ', vous avez ' . $_SESSION['age'] . ' ans.</p>' ;

//Exercice 4
//Vérifier si le login et le mot de passe ont été renseignés
echo '<h2>Exercice 4</h2>';
if(!isset($_COOKIE['UserLogin']) && !isset($_COOKIE['UserPassword'])){
    echo '<p> Vous n\'avez pas renseigné vos informations d\'authentification. 
        Dommage. Revenez à la page d\'accueil et ne touchez pas 10 000$.</p>';
}
else {
    echo '<p>Votre login est : ' . $_COOKIE['UserLogin'] . 
        '</br>Votre password est : ' . $_COOKIE['UserPassword'] . '</p>';
}
?>

<p>Revenir à la <a href="index.php"> page d'accueil.</a></p>