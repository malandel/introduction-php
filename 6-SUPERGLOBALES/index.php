<?php
session_start();
//Valable 24h, httponly (true)
setcookie('UserLogin', $_POST['login'], time() + 24*3600); 
setcookie('UserPassword', $_POST['password'], time() + 24*3600);

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP superglobales</title>
</head>
<body>
    <h2>Exercice 1</h2>
    <?=    
    '<p> 
    User Agent : <strong>' . $_SERVER['HTTP_USER_AGENT'] . '</strong></br>
    IP : <strong>' . $_SERVER['REMOTE_ADDR'] . '</strong></br> 
    Server name : <strong>' . $_SERVER['SERVER_NAME'] . '</strong>
    </p>'; 
    ?>

    <h2>Exercice 2</h2>
    <?php
    $_SESSION['nom'] = 'Lourson';
    $_SESSION['prenom'] = 'Ouïnie';
    $_SESSION['age'] = 42 ;
    ?>
   <p>Afficher les <a href="liens.php">informations secrètes</a><em> * musique mystérieuse *</em></p>

    <h2>Exercice 3</h2>
    <form method="POST">
        <input type="hidden" name="page" value="cookie"/>
        <label for="login">Login :</label>
        <input type="text" name="login" placeholder="Enter your login" required>
        <label for="password">Password :</label>
        <input type="password" name="password" placeholder="Enter your password" required>
        <input type="submit" name="submit">
    </form>
    <p>Afficher vos <a href="liens.php">informations d'authentification.</a></p>
    <p><em>Comment ça c'est pas du tout sécuritaire ?</em></p>
  
    <p>Aller sur la page <a href="modifier.php">Exercice 5</a>pour modifier les cookies.</p>
</body>
</html>