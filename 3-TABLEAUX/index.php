<?php
//Paramètre les messages d'erreur
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function afficher($a){
    echo '<p>' . $a . '</p>';
}
//Exercice 1
$mois = [
   1 => 'Janvier',
   2 => 'Février', 
   3 => 'Mars', 
   4 => 'Avril', 
   5 => 'Mai', 
   6 => 'Juin', 
   7 => 'Juillet', 
   8 => 'Aout',
   9 => 'Septembre',
   10 => 'Octobre',
   11 => 'Novembre',
   12 => 'Décembre',
];
//Exercice 2
afficher($mois[2]);
//Exercice 3
afficher($mois[5]);
//Exercice 4
$mois[7] = 'Août';
//Exercice 7
$mois[13] = 'Glominus';
//Exercice 8
for($i = 1; $i <= count($mois) ; $i++ ){
    afficher($mois[$i]);
}
//Exercice 10
foreach($mois as $key =>$value){
    afficher ('Le mois de ' . ($value) . ' est le numéro ' . ($key));
}

?>