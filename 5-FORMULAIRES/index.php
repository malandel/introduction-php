<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulaires en PHP</title>
</head>
<body>
    <!--Formulaire de l'exercice 1-->
    <h3>Formulaire 1</h3>
    <form action="user.php" method="GET">
        <input type="text" name="prenom1" placeholder="Entrez votre prénom" >
        <input type="text" name="nom1" placeholder="Entrez votre nom" >
        <input type="submit" value="Valider">
    </form>
    <!--Formulaire de l'exercice 2-->
    <h3>Formulaire 2</h3>
    <form action="user.php" method="POST">
        <input type="text" name="prenom2" placeholder="Entrez votre prénom" >
        <input type="text" name="nom2" placeholder="Entrez votre nom" >
        <input type="submit" value="Valider">
    </form>
    <?php
    //Paramètre les messages d'erreur
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    //Exercice 6
    //Récupérer les données transmises via le formulaire 5
    $civilite = $_POST['civilite'];
    $nom5 = $_POST['nom5'];
    $prenom5 = $_POST['prenom5'];
    $nomFichier = $_FILES['fichierUtilisateur']['name'];
    $extFichier = $_FILES['fichierUtilisateur']['type'];
    //S'il n'y a pas de données transmises via le formulaire 5, l'afficher
    if (!isset($civilite, $nom5, $prenom5)){
    //Formulaire de l'exercice 5
        echo '
            <h3>Formulaire 5</h3>
            <form action="." method="POST" enctype="multipart/form-data">
                <select name="civilite" required>
                    <option value="">--Choisissez--</option>
                    <option value="Madame">Madame</option>
                    <option value="Monsieur">Monsieur</option>
                </select>
                <input type="text" name="nom5" placeholder="Entrez votre nom" required>
                <input type="text" name="prenom5" placeholder="Entrez votre prénom" required>
                <input type="file" name="fichierUtilisateur">
                <input type="submit" value="Valider">
            </form>';
    }
    else {
        echo '<p>' . ($civilite) . ' ' . $nom5 . ' ' . $prenom5 . '</p> ';
        echo '<p> votre fichier : ' . $nomFichier . ' ' . $extFichier .  '</p>';
        }
    //Exercice 8 - vérifier si le fichier uploadé est un pdf
    function checkPdf($extFichier){
        if ($extFichier == 'application/pdf'){
            echo '<p>Votre fichier est un pdf</p>';
            return true;
        }
        else {
            return false;
        }
    }
    checkPdf($extFichier);
    //Reset les données pour pouvoir de nouveau afficher le formulaire (test ajout input files)
    ?>
</body>
</html>