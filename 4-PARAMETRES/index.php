<?php
//Paramètre les messages d'erreur
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//Tester si les paramètres existent et les afficher

//Exercice 1
// if (isset($_GET['nom']) && isset($_GET['prenom'])){
//     echo '<p>' . $_GET['nom'] . ' ' . $_GET['prenom'] . '</p>';
// }

//Exercice 2
// if (isset($_GET['age'])){
//     echo '<p>Vous avez ' . $_GET['age'] . ' ans</p>';
// }

//Exercice 3
// if (isset($_GET['dateDebut']) && isset($_GET['dateFin'])){
//     echo '<p>La date de début est ' . $_GET['dateDebut'] . ' et la date de fin est ' . $_GET['dateFin'] . '</p>';
// }

//Exercice 4
// if (isset($_GET['langage']) && isset($_GET['serveur'])){
//     echo '<p>Le langage utilisé est ' . $_GET['langage'] . ' et le serveur est ' . $_GET['serveur'] . '</p>';
// }

//Exercice 5
// if (isset($_GET['semaine'])){
//     echo '<p>Nous sommes à la semaine : ' . $_GET['semaine'] . '</p>';
// }

//Exercice 6
if (isset($_GET['batiment']) && isset($_GET['salle'])){
    echo '<p>Numéro du bâtiment : ' . $_GET['batiment'] . ' / Numéro de la salle : ' . $_GET['salle'] . '</p>';
}
else {
    echo '<p> Attention les paramètres ne sont pas définis </p>';
}

?>

